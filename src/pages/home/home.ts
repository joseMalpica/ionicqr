import { Component } from '@angular/core';
//Plugins
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
//Componentes
import { ToastController }  from "ionic-angular";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor( private barcodeScanner: BarcodeScanner,
               private toastCtrl: ToastController) {

  }

  scan(){
    this.barcodeScanner.scan().then((barcodeData) => {
     console.log("data: ", barcodeData);
    }, (err) => {
        console.log("Error: ", err);
        this.mostrarError( "Error: " + err );
    });
  }

  mostrarError( msg:string ){
    let toast = this.toastCtrl.create({
        message: msg,
        duration: 2500,
        position: 'button'
      });

      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });

      toast.present();
  }

}
